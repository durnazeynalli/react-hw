import backDark from '../assets/icons/dark-theme/backDark.png';
import bookmarkDark from '../assets/icons/dark-theme/bookmarkDark.png';
import calendarDark from '../assets/icons/dark-theme/calendarDark.png';
import chatBubbleDark from '../assets/icons/dark-theme/chat-bubbleDark.png';
import editDark from '../assets/icons/dark-theme/editDark.png';
import heartDark from '../assets/icons/dark-theme/heartDark.png';
import leftAlignDark from '../assets/icons/dark-theme/left-alignDark.png';
import lockDark from '../assets/icons/dark-theme/lockDark.png';
import menuDark from '../assets/icons/dark-theme/menuDark.png';
import moreDark from '../assets/icons/dark-theme/moreDark.png';
import notebookDark from '../assets/icons/dark-theme/notebookDark.png';
import origamiDark from '../assets/icons/dark-theme/origamiDark.png';
import sendDark from '../assets/icons/dark-theme/sendDark.png';
import settingsDark from '../assets/icons/dark-theme/settingsDark.png';
import userDark from '../assets/icons/dark-theme/userDark.png';

export const ICONS_DARK = Object.freeze({
	backDark,
	bookmarkDark,
	calendarDark,
	chatBubbleDark,
	editDark,
	heartDark,
	leftAlignDark,
	lockDark,
	menuDark,
	moreDark,
	notebookDark,
	origamiDark,
	sendDark,
	settingsDark,
	userDark
});
