import backLight from "../assets/icons/light-theme/backLight.png";
import bookmarkLight from "../assets/icons/light-theme/bookmarkLight.png";
import calendarLight from "../assets/icons/light-theme/calendarLight.png";
import chatBubbleLight from "../assets/icons/light-theme/chat-bubbleLight.png";
import editLight from "../assets/icons/light-theme/editLight.png";
import heartLight from "../assets/icons/light-theme/heartLight.png";
import leftAlignLight from "../assets/icons/light-theme/left-alignLight.png";
import lockLight from "../assets/icons/light-theme/lockLight.png";
import menuLight from "../assets/icons/light-theme/menuLight.png";
import moreLight from "../assets/icons/light-theme/moreLight.png";
import notebookLight from "../assets/icons/light-theme/notebookLight.png";
import origamiLight from "../assets/icons/light-theme/origamiLight.png";
import sendLight from "../assets/icons/light-theme/sendLight.png";
import settingsLight from "../assets/icons/light-theme/settingsLight.png";
import userLight from "../assets/icons/light-theme/userLight.png";

export const ICONS_LIGHT = Object.freeze({
	backLight,
	bookmarkLight,
	calendarLight,
	chatBubbleLight,
	editLight,
	heartLight,
	leftAlignLight,
	lockLight,
	menuLight,
	moreLight,
	notebookLight,
	origamiLight,
	sendLight,
	settingsLight,
	userLight
});
